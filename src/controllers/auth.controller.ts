import { NetworkResponseI } from './../domain/response.model';
import { Request, Response } from 'express';
import bcrypt from 'bcryptjs';
import User from '../models/User.model';
import generateJWT from '../utils/generateJWT.utils';
import { UserI } from '../domain/User.model';

interface Token {
	token: string;
}

const login = async (req: Request, res: Response) => {
	let response : NetworkResponseI<Token>;
	try {
		const { email, password } = req.body as UserI;
		const userDb = await User.findOne({email});
		if(!userDb){
			response = {
				success: false,
				message: 'Usuario no existe'
			}
			return res.status(404).send(response);
		}

		//@ts-ignore
		const validPassword = bcrypt.compareSync(password, userDb.password);

		if(!validPassword) {
			response = {
				success: false,
				message: 'Usuario o contraseña incorrectos'
			}
			return res.status(400).send(response);
		}
		const token = await generateJWT(userDb._id) as string;
		response = {
			message: 'Usuario logeado correctamente',
			success: true,
			data: { token }
		}
		return res.send(response);
	} catch (error) {
		response = {
			success: false,
			message: 'Ha ocurrido un problema'
		}
		return res.status(500).send(response);
	}
}

export {
	login
}