import { Request, Response } from 'express';
import Payment from '../models/Payment.model';
import { PaymentI} from '../domain/Payment.model';
import { OrderI} from '../domain/Order.model';
import { networkSuccess, networkError, serverError } from '../middlewares/response.middleware';
import { DishI } from '../domain/Dish.model';


const createPayment = async (request, response) => {
	try {
		const {order, subtotal, total, type} = request.body as PaymentI;
		if(order && subtotal && total && type){
			
			const payment = new Payment(request.body);
			await payment.save();
			response.status(201).send({
				success: true,
				message: 'Pago realizado correctamente'
			});
			return;
		}
		return response.status(400).send({
			success:false,
			message: 'Datos invalidos'
		});
	} catch (error) {
		response.status(500).send({
			error,
			success:false,
			message: 'Ha ocurrido un problema'
		});
	}
}


const getByPaymentId = async (req: Request, res: Response) => {
	try {

		const clientId: string = req.params.id;
		const payment = await Payment.findById(clientId).populate('order');

	return networkSuccess(res, payment, 'Detalle del pago');

	} catch (error) {
		serverError(res, error);
	}
}



export { createPayment, getByPaymentId }