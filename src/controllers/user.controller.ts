import User from '../models/User.model';
import bcrypt from 'bcryptjs';
import { UserI } from '../domain/User.model';

const createUser = async (request, response) => {
	try {
		const {email, name, lastName, password, } = request.body as UserI;
		if(email && name && lastName && password){
			
			const user = new User(request.body);
			
			const salt = bcrypt.genSaltSync();
			//@ts-ignore
			user.password = bcrypt.hashSync(password, salt)

			await user.save();
			response.status(201).send({
				success: true,
				message: 'Usuario creado correctamente'
			});
			return;
		}
		return response.status(400).send({
			success:false,
			message: 'Datos invalidos'
		});
	} catch (error) {
		response.status(500).send({
			error,
			success:false,
			message: 'Ha ocurrido un problema'
		});
	}
}

export { createUser }