import { Request, Response } from 'express';
import Order from '../models/Order.model';
import { networkSuccess, networkError, serverError } from '../middlewares/response.middleware';
import { DishI } from '../domain/Dish.model';

const createOrder = async (req: Request, res: Response) => {
	try {
		const order = new Order(req.body);
		await order.save();
		networkSuccess(res, order, 'Orden creada correctamente');
	} catch (error) {
		serverError(res, error);
	}
}

const getAll = async (req: Request, res: Response) => {
	try {
		const orders = await Order.find().populate('dishes').populate('client');
		networkSuccess(res, orders, 'Lista de ordenes');
	} catch (error) {
		serverError(res, error);
	}
}

const getByClientId = async (req: Request, res: Response) => {
	try {
		const clientId: string = req.params.id;
		const ordersByClient = await Order.find({client: clientId}).populate('dishes');

		const ordersMapped = ordersByClient.map((order: any) => {

			const dishes = order.dishes.map((dish: DishI) => {
				const { isOffert, picture, name, price, _id, description } = dish; 
				return { isOffert, picture, name, price, _id, description }
			})

			return { 
				dishes, 
				id: order._id, 
				quantity: order.quantity
			}

		})

		networkSuccess(res, ordersMapped, '');
	} catch (error) {
		serverError(res, error);
	}
}

export {
	createOrder,
	getAll,
	getByClientId
}