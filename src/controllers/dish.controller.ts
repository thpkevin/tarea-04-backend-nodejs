import { NetworkResponseI } from '../domain/response.model';
import Dish from '../models/Dish.model';
import DishDeleted from '../database/Logic/DishDeleted.model';
import { Response, Request } from 'express';
import { networkError, serverError, networkSuccess } from '../middlewares/response.middleware';
import { DishI } from '../domain/Dish.model';

const create = async (req: Request, res: Response) => {
	try {
		const dish = new Dish(req.body);
		await dish.save();
		networkSuccess(res, dish, 'Plato creado correctamente', 201);
	} catch (error) {
		serverError(res, error);
	}
}

const getAll = async (req: Request, res: Response) => {
	try {
		const dishes = await Dish.find();
		networkSuccess(res, dishes, 'Listado de platos');
	} catch (error) {
		serverError(res, error);
	}
}

const getById = async (req: Request, res: Response) => {
	try {
		const { id } = req.params;
		const dish = await Dish.findById(id);
		if(dish){
			networkSuccess(res, dish, 'Plato encontrado')
		}
		networkError(res, 'Producto no encontrado', 'No se ha encontrado el producto', 404);
	} catch (error) {
		serverError(res, error);
	}
}

const update = async (req: Request, res: Response) => {
	let response: NetworkResponseI;
	try {
		const dish = req.body as DishI;
		await Dish.updateOne({_id: dish._id}, {...dish});
		networkSuccess(res, null, 'Plato actualizado correctamente')
	} catch (error) {
		serverError(res, error);
	}
}

//  : agregar logic delete
const deleteDish = async (req: Request, res: Response) => {
	let response: NetworkResponseI;
	try {
		const id = req.body._id;
		const dishDeleted = await Dish.findOneAndDelete({_id: id});
		console.log("paso1");
		if(!dishDeleted){
			response = {
				success: false,
				message: 'No se ha encontrado el plato'
			};
			res.status(404).send(response);
		}
		console.log("paso2");
		//TODO crear interfaz de Dish
		const {_id,picture, name, price, description, offert, isOffert, delivered } = dishDeleted as DishI; 
		await DishDeleted.create({ 
			_id,
			picture, 
			name, 
			price, 
			description, 
			offert, 
			isOffert, 
			delivered
		});
		console.log({dishDeleted});
		await Dish.deleteOne({_id: id});
		response = {
			success: true,
			message: 'Plato eliminado correctamente',
		};
		res.send(response);
	} catch (error) {
		response = {
			success: false,
			message: 'Ha ocurrido un problema',
			error
		}
		res.status(500).send(response);
	}
}

export { 
	create,
	getAll,
	getById,
	update,
	deleteDish
}