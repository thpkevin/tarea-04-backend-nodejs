export interface UserI {
	_id?: string;
	name: string;
	lastName: string;
	email: string;
	password: string;
	age?: number;
	isSingle?: boolean;
	address: AddressI;
}

export interface AddressI {
	name: string;
	country: string;
}

// OPTINALS
/**
 *  cuando una propiedad es opcional. siempre es indefinida si no ha sido declarada
 * 
 */