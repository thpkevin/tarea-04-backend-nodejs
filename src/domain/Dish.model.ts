import mongoose from 'mongoose';

export interface DishI extends mongoose.Document{
    _id?: String,
    picture: String,
    name: String,
    price: number,
    description: String,
    offert?: Map<any,any>,
    isOffert: boolean,
    delivered: boolean
}
