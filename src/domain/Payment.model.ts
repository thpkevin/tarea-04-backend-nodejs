import mongoose from 'mongoose';

export interface PaymentI extends mongoose.Document{
    _id?: String,
    order: String,
    subtotal: number,
    total: number,
    type: String
}
