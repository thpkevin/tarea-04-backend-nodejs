import mongoose from 'mongoose';

export interface OrderI extends mongoose.Document{
    _id?: String,
    dishes: Map<any,any>,
    quantity: Map<any,any>,
    client: String
}

