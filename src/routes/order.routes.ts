// path: api/order

import { Router } from 'express';
import { createOrder, getAll, getByClientId } from '../controllers/order.controller';

const router = Router();

router.post('/', createOrder);
router.get('/', getAll );
router.get('/client/:id', getByClientId)

export default router;