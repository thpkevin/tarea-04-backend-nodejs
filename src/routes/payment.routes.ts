// path: api/order

import { Router } from 'express';
import { createPayment, getByPaymentId  } from '../controllers/payment.controller';

const router = Router();

router.post('/', createPayment);
// router.get('/', getAll );
router.get('/:id', getByPaymentId)

export default router;