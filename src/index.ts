import express from 'express';
import cors from 'cors';
import { config } from 'dotenv';
import dbConnection from './database/config';
import userRoutes from './routes/user.routes';
import dishRoutes from './routes/dish.routes';
import authRoutes from './routes/auth.routes';
import orderRoutes from './routes/order.routes';
import paymentRoutes from './routes/payment.routes';

config();
dbConnection();

const app = express();


app.use(cors());
app.use(express.json());

app.use('/api/users', userRoutes );
app.use('/api/dish', dishRoutes );
app.use('/api/auth', authRoutes );
app.use('/api/order', orderRoutes );
app.use('/api/payment', paymentRoutes );
// TODO hacer otro route con referencia
app.listen(process.env.PORT, () => {
	console.log(`Server is running on port ${process.env.PORT}`);
})
