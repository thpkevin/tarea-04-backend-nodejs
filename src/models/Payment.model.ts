
import { Schema, model } from 'mongoose';
import { PaymentI } from '../domain/Payment.model';

const Payment = new Schema({
	order: {
		type: Schema.Types.ObjectId, 
		ref: 'orders',
		required: true
	},
	subtotal: {
		type: Number,
		required: true
	},
	total: {
		type: Number,
		required: true
	},
	type: {
		type: String,
		requried: true
	}
});


export default model<PaymentI>('payments',Payment);