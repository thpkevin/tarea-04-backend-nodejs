import { Schema, model } from 'mongoose';

const FoodOrder = new Schema({
	dishes: [{
		type: Schema.Types.ObjectId, 
		ref: 'dish',
		required: true
	}],
	quantity: [{
		type: Number,
		required: true
	}],
	client: {
		type: Schema.Types.ObjectId,
		ref: 'users',
		required: true
	}
});

export default model('orders', FoodOrder);