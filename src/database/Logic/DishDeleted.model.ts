import { Schema, model } from 'mongoose';

const DishSchema = new Schema({
	picture: {
		type: String,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	description: {
		type: String,
		requried: true
	},
	offert: {
		type: Schema.Types.Map,
		required: false
	},
	isOffert: {
		type: Boolean,
		default: false,
		required: true
	},
	delivered: {
		type: Schema.Types.Number,
		default: 0,
		required: true
	}
});

export default model('dishDeleted', DishSchema);